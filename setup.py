from setuptools import setup

setup(
    name = "fptools",
    version = "1.0",
    author = "Steve Harenberg",
    author_email = "harenbergsd@gmail.com",
    description = "A pure python implementation of the FP-growth and FPmax algorithm for mining frequent itemsets (patterns).",
    url = "https://bitbucket.org/harenbergsd/frequent-itemset-mining",
    packages = ['fptools'],
    keywords = ['frequent', 'pattern', 'itemset', 'mining', 'fpgrowth', 'fpmax'],
    license = "CC0",
    download_url = "https://bitbucket.org/harenbergsd/frequent-itemset-mining/get/v1.0.zip"
)
