import unittest
import fptools as fp

class SmallTest(unittest.TestCase):
    def test_fpgrowth(self):
        itemsets = fp.itemsets_from_file("examples/small.txt")
        fis = [itemset for itemset in fp.frequent_itemsets(itemsets, 5)]
        self.assertEqual(fis, [["a"]])
    
    
    def test_fpmax(self):
        itemsets = fp.itemsets_from_file("examples/small.txt")
        
        minsup = 0
        fis = [tuple(sorted(itemset)) for itemset in fp.maximal_frequent_itemsets(itemsets, minsup)]
        expected = set([("a","c","d","e","f")])
        self.assertEqual(set(fis), expected)
        
        minsup = 3
        fis = [tuple(sorted(itemset)) for itemset in fp.maximal_frequent_itemsets(itemsets, minsup)]
        expected = set([("a","c"), ("a","d","e","f")])
        self.assertEqual(set(fis), expected)
        
        minsup = 4
        fis = [tuple(sorted(itemset)) for itemset in fp.maximal_frequent_itemsets(itemsets, minsup)]
        expected = set([("a","c")])
        self.assertEqual(set(fis), expected)


if __name__ == "__main__":
    unittest.main()
